package ar.edu.undec.adapter.service.paciente.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import paciente.exceptions.PacienteYaExisteException;
import paciente.input.CrearPacienteInput;

@RestController
@RequestMapping("pacientes")
public class CrearPacienteController {

    private CrearPacienteInput crearPacienteInput;

    @Autowired
    public CrearPacienteController(CrearPacienteInput crearPacienteInput) {
        this.crearPacienteInput = crearPacienteInput;
    }

    @PostMapping
    public ResponseEntity<?> crearPaciente(@RequestBody CrearPacienteRequestModel crearPacienteRequestModel) {
        try {
            return ResponseEntity.created(null).body(crearPacienteInput.crearPaciente(crearPacienteRequestModel));
        } catch (PacienteYaExisteException e) {
            return ResponseEntity.badRequest().build();
        }
    }
}
