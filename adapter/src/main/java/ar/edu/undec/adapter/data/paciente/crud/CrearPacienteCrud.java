package ar.edu.undec.adapter.data.paciente.crud;
import ar.edu.undec.adapter.data.paciente.model.PacienteEntity;
import org.springframework.data.repository.CrudRepository;

public interface CrearPacienteCrud extends CrudRepository<PacienteEntity, Integer> {

        Boolean existsByDni(String dni);

        Boolean existsByEmail(String email);
}
