package ar.edu.undec.adapter.data.tipopaciente.mapper;

import ar.edu.undec.adapter.data.tipopaciente.model.TipoPacienteEntity;
import tipopaciente.model.TipoPaciente;


public class TipoPacienteDataMapper {
    public static TipoPaciente dataCoreMapper(TipoPacienteEntity tipoPacienteEntity) {
    return TipoPaciente.instancia(tipoPacienteEntity.getId(),
            tipoPacienteEntity.getNombre(),
            tipoPacienteEntity.getDescripcion());
    }

    public static TipoPacienteEntity dataEntityMapper(TipoPaciente tipoPaciente) {
        TipoPacienteEntity tipoPacienteEntity = new TipoPacienteEntity(tipoPaciente.getId(),
                tipoPaciente.getNombre(),
                tipoPaciente.getDescripcion());
        return tipoPacienteEntity;
    }
}