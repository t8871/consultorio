package ar.edu.undec.adapter.data.paciente.model;

import ar.edu.undec.adapter.data.tipopaciente.model.TipoPacienteEntity;

import javax.persistence.*;
import java.time.LocalDate;

import obraSocial.modelo.ObraSocial;



@Entity(name = "paciente")
public class PacienteEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Integer idPaciente;
    private String nombre;
    private String apellido;
    private String nroDocumento;
    private LocalDate fechaNacimiento;
    private String direccion;
    private String telefono;
    private String email;
    private String grupoSanguineo;
    private ObraSocial idObraSocial; //CONSULTAR OBRA SOCIAL
    @JoinColumn(name = "tipo_paciente_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TipoPacienteEntity tipoPaciente;

    public PacienteEntity() {
    }

    public PacienteEntity(Integer idPaciente, String nombre,String apellido,String nroDocumento,
                          LocalDate fechaNacimiento, String direccion, String telefono,String email,
                          String grupoSanguineo, ObraSocial idObraSocial, TipoPacienteEntity tipoPaciente) {
        this.idPaciente = idPaciente;
        this.nombre = nombre;
        this.apellido = apellido;
        this.nroDocumento = nroDocumento;
        this.fechaNacimiento = fechaNacimiento;
        this.direccion = direccion;
        this.telefono = telefono;
        this.email = email;
        this.grupoSanguineo = grupoSanguineo;
        this.idObraSocial = idObraSocial;
        this.tipoPaciente = tipoPaciente;
    }

    public Integer getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(Integer idPaciente) {
        this.idPaciente = idPaciente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGrupoSanguineo() {
        return grupoSanguineo;
    }

    public void setGrupoSanguineo(String grupoSanguineo) {
        this.grupoSanguineo = grupoSanguineo;
    }

    public ObraSocial getIdObraSocial() {
        return idObraSocial;
    }

    public void setIdObraSocial(ObraSocial idObraSocial) {
        this.idObraSocial = idObraSocial;
    }

    public TipoPacienteEntity getTipoPaciente() {
        return tipoPaciente;
    }

    public void setTipoPaciente(TipoPacienteEntity tipoPaciente) {
        this.tipoPaciente = tipoPaciente;
    }
}
