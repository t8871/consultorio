package ar.edu.undec.adapter.data.paciente.repoimplementacion;

import ar.edu.undec.adapter.data.paciente.crud.CrearPacienteCrud;
import ar.edu.undec.adapter.data.paciente.mapper.PacienteDataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import paciente.modelo.Paciente;
import paciente.output.CrearPacienteRepository;

public class CrearPacienteRepoImplementacion implements CrearPacienteRepository {

    private CrearPacienteCrud crearPacienteCrud;

    @Autowired
    public CrearPacienteRepoImplementacion(CrearPacienteCrud crearPacienteCrud) {
        this.crearPacienteCrud = crearPacienteCrud;
    }

    @Override
    public boolean existePorDNI(String dni) {
        return crearPacienteCrud.existsByDni(dni);
    }

    @Override
    public boolean existePorEmail(String email) {
        return crearPacienteCrud.existsByEmail(email);
    }

    @Override
    public Integer guardarPaciente(Paciente paciente) {
        return PacienteDataMapper.dataEntityMapper(paciente).getIdPaciente();
    }
}
