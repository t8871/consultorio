package ar.edu.undec.adapter.data.paciente.exception;

public class PacienteMapeoException extends RuntimeException {
    public PacienteMapeoException(String message) {
        super(message);
    }
}
