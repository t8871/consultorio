package ar.edu.undec.adapter.service.tipopaciente.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TipoPacienteBeanConfig {

    @Bean
    public ConsultarTipoPacientePorIdUseCase consultarTipoPacientePorIdUseCase(ConsultarTipoPacientePorIdRepository consultarTipoPacientePorIdRepository) {
        return new ConsultarTipoPacientePorIdUseCase(consultarTipoPacientePorIdRepository);
    }
}
