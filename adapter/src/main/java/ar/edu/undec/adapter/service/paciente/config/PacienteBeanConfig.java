package ar.edu.undec.adapter.service.paciente.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import paciente.output.CrearPacienteRepository;
import paciente.usecasepaciente.CrearPacienteUseCase;

@Configuration
public class PacienteBeanConfig {

    @Bean
    public CrearPacienteUseCase crearPacienteUseCase(CrearPacienteRepository crearPacienteRepository, ConsultarTipoPacientePorIdUseCase consultarTipoPacientePorIdUseCase) {
        return new CrearPacienteUseCase(crearPacienteRepository, consultarTipoPacientePorIdUseCase);
    }
}