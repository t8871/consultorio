package ar.edu.undec.adapter.data.tipopaciente.crud;

import ar.edu.undec.adapter.data.tipopaciente.model.TipoPacienteEntity;
import org.springframework.data.repository.CrudRepository;
import tipopaciente.model.TipoPersona;

public interface CrearTipoPacienteCrud extends CrudRepository<TipoPacienteEntity, Integer> {
}
