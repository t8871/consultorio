package ar.edu.undec.adapter.data.paciente.mapper;

import ar.edu.undec.adapter.data.paciente.exception.PacienteMapeoException;
import ar.edu.undec.adapter.data.paciente.model.PacienteEntity;
import ar.edu.undec.adapter.data.tipopaciente.mapper.TipoPacienteDataMapper;
import paciente.modelo.Paciente;


public class PacienteDataMapper {
    public static Paciente dataCoreMapper(PacienteEntity pacienteEntity) {
    try {
        return Paciente.instancia(pacienteEntity.getIdPaciente(),
                pacienteEntity.getNroDocumento(),
                pacienteEntity.getNombre(),
                pacienteEntity.getEmail(),
                pacienteEntity.getTelefono(),
                pacienteEntity.getFechaNacimiento(),
                TipoPacienteDataMapper.dataCoreMapper((pacienteEntity.getTipoPaciente()))); //CONSULTAR
    } catch (Exception e) {
        throw new PacienteMapeoException("Error al mapear paciente de Entidad a Core");
    }
}

    public static PacienteEntity dataEntityMapper(Paciente paciente) {
        try{
            return new PacienteEntity(paciente.getIdPaciente(),
                    paciente.getNombre(),
                    paciente.getApellido();
                    paciente.getNroDocumento(), paciente.getGrupoSanguineo(),
                    paciente.getEmail(),
                    paciente.getTelefono(),
                    paciente.,
                    TipoPacienteDataMapper.dataEntityMapper(paciente.getTipoPaciente()));
        } catch (Exception e) {
            throw new PacienteMapeoException("Error al mapear Paciente de Core a Entidad");
        }
    }
}