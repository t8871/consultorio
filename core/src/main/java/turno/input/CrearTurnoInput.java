package turno.input;

import turno.modelo.Turno;

public interface CrearTurnoInput {
    Integer crearTurno(Turno miTurno);
}
