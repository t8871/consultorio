package turno.input;

import turno.modelo.Turno;

import java.util.List;

public interface ConsultarTurnoInput {
    List<Turno> obtenerTurno();
}
