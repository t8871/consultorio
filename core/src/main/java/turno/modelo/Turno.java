package turno.modelo;

import turno.exceptions.TurnoIncompletoExceptions;

import java.time.LocalDate;
import java.time.LocalTime;

public class Turno {
    private Integer idTurno;
    private Integer idPaciente;
    private Integer matricula;
    private LocalDate diaTurno;
    private LocalTime horaDesde;
    private LocalTime horaHasta;

    public Turno(Integer idTurno, Integer idPaciente,
                 Integer matricula, LocalDate diaTurno, LocalTime horaDesde, LocalTime horaHasta) {
        this.idTurno = idTurno;
        this.idPaciente = idPaciente;
        this.matricula = matricula;
        this.diaTurno = diaTurno;
        this.horaDesde = horaDesde;
        this.horaHasta = horaHasta;
    }

    public static Turno instancia(Integer idTurno, Integer idPaciente,
                                  Integer matricula, LocalDate diaTurno, LocalTime horaDesde, LocalTime horaHasta ) {

        if(idPaciente== null){
            throw new TurnoIncompletoExceptions("El id del paciente es obligatorio");
        }
        if(matricula== null){
            throw new TurnoIncompletoExceptions("La matricula del medico es obligatoria");
        }
       //VALIDAR FECHAS Y HORA
        return new Turno(idTurno, idPaciente, matricula, diaTurno, horaDesde, horaHasta);
    }
    public Integer getIdTurno() {
        return idTurno;
    }

    public Integer getIdPaciente() {
        return idPaciente;
    }

    public Integer getMatricula() {
        return matricula;
    }

    public LocalDate getDiaTurno() {
        return diaTurno;
    }

    public LocalTime getHoraDesde() {
        return horaDesde;
    }

    public LocalTime getHoraHasta() {
        return horaHasta;
    }
}
