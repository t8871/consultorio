package turno.usecaseturno;

import turno.input.CrearTurnoInput;
import turno.modelo.Turno;
import turno.output.CrearTurnoRepository;

public class CrearTurnoUseCase implements CrearTurnoInput {
    private CrearTurnoRepository crearTurnoRepository;
    public CrearTurnoUseCase(CrearTurnoRepository crearTurnoRepository){

    }

    @Override
    public Integer crearTurno(Turno miTurno) {
        return crearTurnoRepository.guardarTurno(miTurno);
    }
}