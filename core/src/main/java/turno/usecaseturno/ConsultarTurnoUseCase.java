package turno.usecaseturno;

import turno.input.ConsultarTurnoInput;
import turno.modelo.Turno;
import turno.output.ConsultarTurnoRepository;

import java.util.List;

public class ConsultarTurnoUseCase implements ConsultarTurnoInput {
    private ConsultarTurnoRepository consultarTurnoRepository;
    public ConsultarTurnoUseCase(ConsultarTurnoRepository consultarTurnoRepository){
        this.consultarTurnoRepository = consultarTurnoRepository;
    }
    @Override
    public List<Turno> obtenerTurno(){return (List<Turno>) consultarTurnoRepository.obtenerTurno();}
}