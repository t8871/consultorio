package turno.output;

import turno.modelo.Turno;

import java.util.Collection;

public interface ConsultarTurnoRepository {
    Collection<Turno> obtenerTurno();
}
