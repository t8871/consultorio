package turno.output;


import turno.modelo.Turno;

public interface CrearTurnoRepository {
    boolean existePorIdTurno(Integer idTurno);

    Integer guardarTurno(Turno miTurno);
}
