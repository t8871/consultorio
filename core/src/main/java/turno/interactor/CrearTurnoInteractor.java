package turno.interactor;

import turno.exceptions.TurnoYaExisteExceptions;
import turno.input.CrearTurnoInput;
import turno.modelo.Turno;
import turno.output.CrearTurnoRepository;

public class CrearTurnoInteractor implements CrearTurnoInput {

    private CrearTurnoRepository crearTurnoRepository;

    public CrearTurnoInteractor(CrearTurnoRepository crearTurnoRepository) {
        this.crearTurnoRepository = crearTurnoRepository;
    }

    @Override
    public Integer crearTurno(Turno miTurno) {
        if(crearTurnoRepository.existePorIdTurno(miTurno.getIdTurno())){
            throw new TurnoYaExisteExceptions(String.format("El turno ya existe"));
        }
        Integer nuevoTurno = crearTurnoRepository.guardarTurno(miTurno);
        return nuevoTurno;
    }
}
