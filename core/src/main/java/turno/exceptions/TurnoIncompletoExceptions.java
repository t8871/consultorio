package turno.exceptions;

public class TurnoIncompletoExceptions extends RuntimeException {
    public TurnoIncompletoExceptions(String message) {
        super(message);
        return;
    }
}