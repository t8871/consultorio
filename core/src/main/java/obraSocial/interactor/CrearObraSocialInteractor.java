package obraSocial.interactor;

import obraSocial.exceptions.ObraSocialYaExisteExceptions;
import obraSocial.input.CrearObraSocialInput;
import obraSocial.modelo.ObraSocial;
import obraSocial.output.CrearObraSocialRepository;

public class CrearObraSocialInteractor implements CrearObraSocialInput {

    private CrearObraSocialRepository crearObraSocialRepository;

    public CrearObraSocialInteractor(CrearObraSocialRepository crearObraSocialRepository) {
        this.crearObraSocialRepository = crearObraSocialRepository;
    }

    @Override
    public Integer crearObraSocial(ObraSocial miObraSocial) {
        if(crearObraSocialRepository.existePorNombre("Galeno")){
            throw new ObraSocialYaExisteExceptions(String.format("La obra social ya existe"));
        }
        Integer nuevaObraSocial = crearObraSocialRepository.guardarObraSocial(miObraSocial);
        return nuevaObraSocial;
    }

}