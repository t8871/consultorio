package obraSocial.input;


import obraSocial.modelo.ObraSocial;

import java.util.List;

public interface ConsultarObraSocialInput {
    List<ObraSocial> obtenerObraSocial();
}
