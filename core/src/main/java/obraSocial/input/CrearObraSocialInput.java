package obraSocial.input;

import obraSocial.modelo.ObraSocial;

public interface CrearObraSocialInput {
    Integer crearObraSocial(ObraSocial miObraSocia);
}
