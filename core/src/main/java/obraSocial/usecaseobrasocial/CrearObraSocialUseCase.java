package obraSocial.usecaseobrasocial;

import obraSocial.input.CrearObraSocialInput;
import obraSocial.modelo.ObraSocial;
import obraSocial.output.CrearObraSocialRepository;

public class CrearObraSocialUseCase implements CrearObraSocialInput {
    private CrearObraSocialRepository crearObraSocialRepository;
    public CrearObraSocialUseCase(CrearObraSocialRepository crearObraSocialRepository){

    }

    @Override
    public Integer crearObraSocial(ObraSocial miObraSocial) {
        return crearObraSocialRepository.guardarObraSocial(miObraSocial);
    }
}