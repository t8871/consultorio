package obraSocial.usecaseobrasocial;


import obraSocial.input.ConsultarObraSocialInput;
import obraSocial.modelo.ObraSocial;
import obraSocial.output.ConsultarObraSocialRepository;

import java.util.List;

public class ConsultarObraSocialUseCase implements ConsultarObraSocialInput {
    private ConsultarObraSocialRepository consultarObraSocialRepository;
    public ConsultarObraSocialUseCase(ConsultarObraSocialRepository consultarObraSocialRepository){
        this.consultarObraSocialRepository = consultarObraSocialRepository;
    }
    @Override
    public List<ObraSocial> obtenerObraSocial(){return (List<ObraSocial>) consultarObraSocialRepository.obtenerObraSocial();}
}