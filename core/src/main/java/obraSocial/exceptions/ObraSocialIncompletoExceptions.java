package obraSocial.exceptions;

public class ObraSocialIncompletoExceptions extends RuntimeException{
    public ObraSocialIncompletoExceptions(String message){
        super(message);
        return;
    }
}
