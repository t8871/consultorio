package obraSocial.modelo;

import obraSocial.exceptions.ObraSocialIncompletoExceptions;

public class ObraSocial {
    private Integer idObraSocial;
    private String nombreObraSocial;
    private String nroDeAfiliado;
    private String tipoDePlan;
    private String telefono;
    private String email;
    private String direccion;

    public ObraSocial(Integer idObraSocial, String nombreObraSocial,
                      String nroDeAfiliado, String tipoDePlan, String telefono, String email, String direccion) {
        this.idObraSocial = idObraSocial;
        this.nombreObraSocial = nombreObraSocial;
        this.nroDeAfiliado = nroDeAfiliado;
        this.tipoDePlan = tipoDePlan;
        this.telefono = telefono;
        this.email = email;
        this.direccion = direccion;
    }
    public static ObraSocial instancia(Integer idObraSocial, String nombreObraSocial,
                                       String nroDeAfiliado, String tipoDePlan, String telefono,
                                       String email, String direccion) {
        if(nombreObraSocial== null ||nombreObraSocial.isEmpty()){
            throw new ObraSocialIncompletoExceptions("El nombre de la obra social es obligatorio");
        }
        if(nroDeAfiliado== null || nroDeAfiliado.isEmpty()){
            throw new ObraSocialIncompletoExceptions("El numero de afiliado es obligatorio");
        }
        if(tipoDePlan== null || tipoDePlan.isEmpty()){
            throw new ObraSocialIncompletoExceptions("El tipo de plan es obligatorio");
        }
        if(telefono== null || telefono.isEmpty()){
            throw new ObraSocialIncompletoExceptions("El telefono es obligatoria");
        }
        if(email== null || email.isEmpty()){
            throw new ObraSocialIncompletoExceptions("El email es obligatorio");
        }
        if(direccion== null || direccion.isEmpty()){
            throw new ObraSocialIncompletoExceptions("La direccion es obligatoria");
        }

        return new ObraSocial(idObraSocial, nombreObraSocial, nroDeAfiliado, tipoDePlan, telefono, email, direccion);
    }
    public Integer getIdObraSocial() {
        return idObraSocial;
    }

    public String getNombreObraSocial() {
        return nombreObraSocial;
    }

    public String getNroDeAfiliado() {
        return nroDeAfiliado;
    }

    public String getTipoDePlan() {
        return tipoDePlan;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getEmail() {
        return email;
    }

    public String getDirección() {
        return direccion;
    }
}
