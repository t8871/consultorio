package obraSocial.output;

import obraSocial.modelo.ObraSocial;

import java.util.Collection;

public interface ConsultarObraSocialRepository {
    Collection<ObraSocial> obtenerObraSocial();
}
