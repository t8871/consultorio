package obraSocial.output;

import obraSocial.modelo.ObraSocial;

public interface CrearObraSocialRepository {
    boolean existePorNombre(String nombreObraSocial);

    Integer guardarObraSocial(ObraSocial miObraSocial);
}
