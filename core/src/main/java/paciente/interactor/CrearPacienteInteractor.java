package paciente.interactor;

import paciente.exceptions.PacienteYaExisteException;
import paciente.input.CrearPacienteInput;
import paciente.modelo.Paciente;
import paciente.output.CrearPacienteRepository;

public class CrearPacienteInteractor implements CrearPacienteInput {

    private CrearPacienteRepository crearPacienteRepository;

    public CrearPacienteInteractor(CrearPacienteRepository crearPacienteRepository) {
        this.crearPacienteRepository = crearPacienteRepository;
    }

    @Override
    public Integer crearPaciente(Paciente miPaciente) {
        if(crearPacienteRepository.existePorDNI(miPaciente.getNroDocumento())){
            throw new PacienteYaExisteException(String.format("El paciente con documento numero %s ya existe", miPaciente.getNroDocumento()));
        }
       Integer nuevoPaciente = crearPacienteRepository.guardarPaciente(miPaciente);
        return nuevoPaciente;
    }
}
