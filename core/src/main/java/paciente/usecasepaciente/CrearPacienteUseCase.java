package paciente.usecasepaciente;

import paciente.input.CrearPacienteInput;
import paciente.modelo.Paciente;
import paciente.output.CrearPacienteRepository;

public class CrearPacienteUseCase implements CrearPacienteInput {
    private CrearPacienteRepository crearPacienteRepository;
    public CrearPacienteUseCase(CrearPacienteRepository crearPacienteRepository){

    }

    @Override
    public Integer crearPaciente(Paciente miPaciente) {
        return crearPacienteRepository.guardarPaciente(miPaciente);
    }
}