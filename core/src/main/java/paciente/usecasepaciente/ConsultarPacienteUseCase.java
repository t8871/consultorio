package paciente.usecasepaciente;

import paciente.input.ConsultarPacienteInput;
import paciente.modelo.Paciente;
import paciente.output.ConsultarPacienteRepository;

import java.util.List;

public class ConsultarPacienteUseCase implements ConsultarPacienteInput {
    private ConsultarPacienteRepository consultarPacienteRepository;
    public ConsultarPacienteUseCase(ConsultarPacienteRepository consultarPacienteRepository){
        this.consultarPacienteRepository = consultarPacienteRepository;
    }
    @Override
    public List<Paciente> obtenerPaciente(){return (List<Paciente>) consultarPacienteRepository.obtenerPaciente();}
}