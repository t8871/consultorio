package paciente.input;
import paciente.modelo.Paciente;
public interface CrearPacienteInput {
    Integer crearPaciente(Paciente miPaciente);
}
