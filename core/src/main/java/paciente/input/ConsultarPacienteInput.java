package paciente.input;

import paciente.modelo.Paciente;

import java.util.List;

public interface ConsultarPacienteInput {
    List<Paciente> obtenerPaciente();
}
