package paciente.exceptions;

public class PacienteIncompletoExceptions extends RuntimeException{
    public PacienteIncompletoExceptions(String message){
        super(message);
        return;
    }
}
