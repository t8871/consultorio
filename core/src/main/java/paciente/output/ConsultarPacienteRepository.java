package paciente.output;

import paciente.modelo.Paciente;

import java.util.Collection;

public interface ConsultarPacienteRepository {
    Collection<Paciente> obtenerPaciente();
}
