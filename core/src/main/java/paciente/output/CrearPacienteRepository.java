package paciente.output;

import paciente.modelo.Paciente;

public interface CrearPacienteRepository {
    boolean existePorDNI(String nroDocumento);

    Integer guardarPaciente(Paciente miPaciente);
}
