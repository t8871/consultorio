package paciente.modelo;

import obraSocial.modelo.ObraSocial;
import paciente.exceptions.PacienteIncompletoExceptions;

import java.time.LocalDate;

public class Paciente {
    private Integer idPaciente;
    private String nombre;
    private String apellido;
    private String nroDocumento;
    private LocalDate fechaNacimiento;
    private String direccion;
    private String telefono;
    private String email;
    private String grupoSanguineo;
    private ObraSocial idObraSocial;

    private Paciente(Integer idPaciente, String nombre, String apellido, String nroDocumento, LocalDate fechaNacimiento,
                    String direccion, String telefono, String email, String grupoSanguineo, ObraSocial miObraSocial) {
        this.idPaciente = idPaciente;
        this.nombre = nombre;
        this.apellido = apellido;
        this.nroDocumento = nroDocumento;
        this.fechaNacimiento = fechaNacimiento;
        this.direccion = direccion;
        this.telefono = telefono;
        this.email = email;
        this.grupoSanguineo = grupoSanguineo;
    }

    public static Paciente instancia(Integer idPaciente, String nombre, String apellido, String nroDocumento,
                                     LocalDate fechaNacimiento, String direccion, String telefono,
                                     String email, String grupoSanguineo,  ObraSocial miObraSocial) {
        if(nombre== null || nombre.isEmpty()){
            throw new PacienteIncompletoExceptions("El nombre del paciente es obligatorio");
        }
        if(apellido== null || apellido.isEmpty()){
            throw new PacienteIncompletoExceptions("El apellido del paciente es obligatorio");
        }
        if(nroDocumento== null || nroDocumento.isEmpty()){
            throw new PacienteIncompletoExceptions("El numero de documento del paciente es obligatorio");
        }
        if(direccion== null || direccion.isEmpty()){
            throw new PacienteIncompletoExceptions("La direccion del paciente es obligatoria");
        }
        if(grupoSanguineo== null || grupoSanguineo.isEmpty()){
            throw new PacienteIncompletoExceptions("El grupo sanguineo del paciente es obligatorio");
        }

        return new Paciente(idPaciente, nombre, apellido, nroDocumento, fechaNacimiento, direccion, telefono, email, grupoSanguineo, miObraSocial);
    }


    public Integer getIdPaciente() {
        return idPaciente;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getEmail() {
        return email;
    }

    public String getGrupoSanguineo() {
        return grupoSanguineo;
    }

}
