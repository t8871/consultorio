package medico.output;

import medico.modelo.Medico;

public interface CrearMedicoRepository {
    boolean existePorMatricula(Integer matricula);

    Integer guardarMedico(Medico miMedico);
}
