package medico.output;

import medico.modelo.Medico;

import java.util.Collection;

public interface ConsultarMedicoRepository {
    Collection<Medico> obtenerMedico();
}
