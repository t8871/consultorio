package medico.modelo;

import medico.exceptions.MedicoIncompletoExceptions;
import paciente.exceptions.PacienteIncompletoExceptions;
import paciente.modelo.Paciente;

import java.time.LocalDate;
import java.util.Objects;

public class Medico {
    private Integer matricula;
    private String nombre;
    private String apellido;
    private String direccion;
    private String telefono;
    private String email;
    private String especialidad;

    private Medico(Integer matricula, String nombre, String apellido,
                  String direccion, String telefono, String email, String especialidad) {
        this.matricula = matricula;
        this.nombre = nombre;
        this.apellido = apellido;
        this.direccion = direccion;
        this.telefono = telefono;
        this.email = email;
        this.especialidad = especialidad;
    }
    public static Medico instancia(Integer matricula, String nombre, String apellido,
                                   String direccion, String telefono, String email, String especialidad) {

        if(nombre== null || nombre.isEmpty()){
            throw new MedicoIncompletoExceptions("El nombre del medico es obligatorio");
        }
        if(apellido== null || apellido.isEmpty()){
            throw new MedicoIncompletoExceptions("El apellido del medico es obligatorio");
        }
        if(direccion== null || direccion.isEmpty()){
            throw new MedicoIncompletoExceptions("La direccion del medico es obligatoria");
        }
        if(telefono== null || telefono.isEmpty()){
            throw new MedicoIncompletoExceptions("La direccion del medico es obligatorio");
        }
        if(especialidad== null || especialidad.isEmpty()){
            throw new MedicoIncompletoExceptions("La direccion del medico es obligatoria");
        }

       return new Medico(matricula, nombre,apellido, direccion, telefono, email, especialidad);
    }
    public Integer getMatricula() {
        return matricula;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getEmail() {
        return email;
    }

    public String getEspecialidad() {
        return especialidad;
    }

}
