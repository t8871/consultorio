package medico.usecasemedico;

import medico.input.ConsultarMedicoInput;
import medico.modelo.Medico;
import medico.output.ConsultarMedicoRepository;

import java.util.List;

public class ConsultarMedicoUseCase implements ConsultarMedicoInput {
    private ConsultarMedicoRepository consultarMedicoRepository;
    public ConsultarMedicoUseCase(ConsultarMedicoRepository consultarMedicoRepository){
        this.consultarMedicoRepository = consultarMedicoRepository;
    }
    @Override
    public List<Medico> obtenerMedico(){return (List<Medico>) consultarMedicoRepository.obtenerMedico();}
}
