package medico.usecasemedico;

import medico.input.CrearMedicoInput;
import medico.modelo.Medico;
import medico.output.CrearMedicoRepository;

public class CrearMedicoUseCase implements CrearMedicoInput {
    private CrearMedicoRepository crearMedicoRepository;
    public CrearMedicoUseCase(CrearMedicoRepository crearMedicoRepository){

    }

    @Override
    public Integer crearMedico(Medico miMedico) {
        return crearMedicoRepository.guardarMedico(miMedico);
    }
}
