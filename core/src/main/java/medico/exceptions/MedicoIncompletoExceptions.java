package medico.exceptions;

public class MedicoIncompletoExceptions extends RuntimeException{
    public MedicoIncompletoExceptions(String message){
        super(message);
        return;
    }
}
