package medico.interactor;

import medico.exceptions.MedicoYaExisteException;
import medico.input.CrearMedicoInput;
import medico.modelo.Medico;
import medico.output.CrearMedicoRepository;

public class CrearMedicoInteractor implements CrearMedicoInput {
    private CrearMedicoRepository crearMedicoRepository;

    public CrearMedicoInteractor(CrearMedicoRepository crearMedicoRepository) {
        this.crearMedicoRepository = crearMedicoRepository;
    }

    @Override
    public Integer crearMedico(Medico miMedico) {
        if(crearMedicoRepository.existePorMatricula(miMedico.getMatricula())){
            throw new MedicoYaExisteException(String.format("El medico con matricula número %s ya existe", miMedico.getMatricula()));
        }
        Integer nuevoMedico = crearMedicoRepository.guardarMedico(miMedico);
        return nuevoMedico;
    }

}
