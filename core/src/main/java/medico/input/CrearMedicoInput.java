package medico.input;
import medico.modelo.Medico;
public interface CrearMedicoInput {
    Integer crearMedico(Medico miMedico);
}
