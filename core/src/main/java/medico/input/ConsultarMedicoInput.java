package medico.input;

import medico.modelo.Medico;

import java.util.List;

public interface ConsultarMedicoInput {
    List<Medico> obtenerMedico();
}
