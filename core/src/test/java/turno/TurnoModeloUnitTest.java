package turno;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import turno.modelo.Turno;

import java.time.LocalDate;
import java.time.LocalTime;

public class TurnoModeloUnitTest {

    private Integer idTurno;
    private Integer idPaciente;
    private Integer matricula;
    private LocalDate diaTurno;
    private LocalTime horaDesde;
    private LocalTime horaHasta;

    @Test
    void instancia_TodosLosAtributosCorrectos_InstanciaTurno(){
        //Arrange
        //Act
        Turno miTurno = Turno.instancia(2527,1234,6541, LocalDate.of(2022,04,20),
                LocalTime.of(10,30), LocalTime.of(10,45));
//CONSULTAR HORA HASTA
        //Assert
        Assertions.assertNotNull(miTurno);
        Assertions.assertEquals(2527, miTurno.getIdTurno());
        Assertions.assertEquals(1234, miTurno.getIdPaciente());
        Assertions.assertEquals(6541, miTurno.getMatricula());
        Assertions.assertEquals(2022/04/20, miTurno.getDiaTurno());
        Assertions.assertEquals(10.30, miTurno.getHoraDesde());
        Assertions.assertEquals(10.45, miTurno.getHoraHasta());

    }

}
