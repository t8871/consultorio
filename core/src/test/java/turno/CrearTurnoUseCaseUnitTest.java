package turno;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import turno.exceptions.TurnoYaExisteExceptions;
import turno.input.CrearTurnoInput;
import turno.interactor.CrearTurnoInteractor;
import turno.modelo.Turno;
import turno.output.CrearTurnoRepository;
import java.time.LocalDate;
import java.time.LocalTime;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CrearTurnoUseCaseUnitTest {
    @Mock
    CrearTurnoRepository crearTurnoRepository;
    @Test
    void creaTurno_TurnoNoExiste_CreaTurno(){
        Turno miTurno = Turno.instancia(10514,123,568,LocalDate.of(2022,5,16), LocalTime.of(12,30),LocalTime.of(13,00));
        CrearTurnoInput crearTurnoInteractor = new CrearTurnoInteractor(crearTurnoRepository);

        when(crearTurnoRepository.existePorIdTurno(10514)).thenReturn(false);
        when(crearTurnoRepository.guardarTurno(miTurno)).thenReturn(2);

        Assertions.assertEquals(2,crearTurnoInteractor.crearTurno(miTurno));
    }
    @Test
    void creaTurno_TurnoYaExiste_TurnoYaExisteException(){
        Turno miTurno = Turno.instancia(10514,123,568,LocalDate.of(2022,5,16), LocalTime.of(12,30),LocalTime.of(13,00));
        CrearTurnoInput crearTurnoInteractor = new CrearTurnoInteractor(crearTurnoRepository);

        verify(crearTurnoRepository, never()).guardarTurno(miTurno);

        when(crearTurnoRepository.existePorIdTurno(10514)).thenReturn(true);

        Assertions.assertThrows(TurnoYaExisteExceptions.class,()-> crearTurnoInteractor.crearTurno(miTurno));
    }
}
