package obraSocial;

import obraSocial.exceptions.ObraSocialIncompletoExceptions;
import obraSocial.modelo.ObraSocial;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import paciente.exceptions.PacienteIncompletoExceptions;
import paciente.modelo.Paciente;

import java.time.LocalDate;

public class ObraSocialModeloUnitTest {
    @Test
    void instancia_TodosLosAtributosCorrectos_InstanciaObraSocial(){
        //Arrange
        //Act
        ObraSocial miObraSocial = ObraSocial.instancia(1234,"OSDE",
                "36965115-0", "410","4319531",
                "miobrasocial@osde.com","Belgrano 1236" );

        //Assert
        Assertions.assertNotNull(miObraSocial);
        Assertions.assertEquals(1234, miObraSocial.getIdObraSocial());
        Assertions.assertEquals("OSDE", miObraSocial.getNombreObraSocial());
        Assertions.assertEquals("36965115-0", miObraSocial.getNroDeAfiliado());
        Assertions.assertEquals("410", miObraSocial.getTipoDePlan());
        Assertions.assertEquals("4319531", miObraSocial.getTelefono());
        Assertions.assertEquals("miobrasocial@osde.com", miObraSocial.getEmail());
        Assertions.assertEquals("Belgrano 1236", miObraSocial.getDirección());

    }

    @Test
    void instancia_FaltaNombre_InstanciaObraSocial(){
        //Arrange
        //Act
        Exception exceptionVacio = Assertions.assertThrows(ObraSocialIncompletoExceptions.class,() -> ObraSocial.instancia(1234,"",
                "36965115-0", "410","4319531",
                "miobrasocial@osde.com","Belgrano 1236" ));
        Exception exceptionNull = Assertions.assertThrows(PacienteIncompletoExceptions.class,() -> ObraSocial.instancia(1234,null,
                "36965115-0", "410","4319531",
                "miobrasocial@osde.com","Belgrano 1236" ));
        //Assert
        Assertions.assertEquals("El nombre de la obra social es obligatorio", exceptionVacio.getMessage());
        Assertions.assertEquals("El nombre de la obra social es obligatorio", exceptionNull.getMessage());
    }
    @Test
    void instancia_FaltaNumeroDeAfiliado_InstanciaObraSocial(){
        //Arrange
        //Act
        Exception exceptionVacio = Assertions.assertThrows(ObraSocialIncompletoExceptions.class,() -> ObraSocial.instancia(1234,"OSDE",
                "", "410","4319531",
                "miobrasocial@osde.com","Belgrano 1236" ));
        Exception exceptionNull = Assertions.assertThrows(PacienteIncompletoExceptions.class,() -> ObraSocial.instancia(1234,"OSDE",
                null, "410","4319531",
                "miobrasocial@osde.com","Belgrano 1236" ));
        //Assert
        Assertions.assertEquals("El numero de afiliado es obligatorio", exceptionVacio.getMessage());
        Assertions.assertEquals("El numero de afiliado es obligatorio", exceptionNull.getMessage());
    }

    @Test
    void instancia_FaltaTipoDePlan_InstanciaObraSocial(){
        //Arrange
        //Act
        Exception exceptionVacio = Assertions.assertThrows(ObraSocialIncompletoExceptions.class,() -> ObraSocial.instancia(1234,"OSDE",
                "36965115-0", "","4319531",
                "miobrasocial@osde.com","Belgrano 1236" ));
        Exception exceptionNull = Assertions.assertThrows(PacienteIncompletoExceptions.class,() -> ObraSocial.instancia(1234,"OSDE",
                "36965115-5", null,"4319531",
                "miobrasocial@osde.com","Belgrano 1236" ));
        //Assert
        Assertions.assertEquals("El tipo de plan es obligatorio", exceptionVacio.getMessage());
        Assertions.assertEquals("El tipo de plan es obligatorio", exceptionNull.getMessage());
    }
    @Test
    void instancia_FaltaTelefono_InstanciaObraSocial(){
        //Arrange
        //Act
        Exception exceptionVacio = Assertions.assertThrows(ObraSocialIncompletoExceptions.class,() -> ObraSocial.instancia(1234,"OSDE",
                "36965115-0", "410","",
                "miobrasocial@osde.com","Belgrano 1236" ));
        Exception exceptionNull = Assertions.assertThrows(PacienteIncompletoExceptions.class,() -> ObraSocial.instancia(1234,"OSDE",
                "36965115-5", "410",null,
                "miobrasocial@osde.com","Belgrano 1236" ));
        //Assert
        Assertions.assertEquals("El numero de telefono es obligatorio", exceptionVacio.getMessage());
        Assertions.assertEquals("El numero de telefono es obligatorio", exceptionNull.getMessage());
    }

    @Test
    void instancia_FaltaEmail_InstanciaObraSocial(){
        //Arrange
        //Act
        Exception exceptionVacio = Assertions.assertThrows(ObraSocialIncompletoExceptions.class,() -> ObraSocial.instancia(1234,"OSDE",
                "36965115-0", "410","4319531",
                "","Belgrano 1236" ));
        Exception exceptionNull = Assertions.assertThrows(PacienteIncompletoExceptions.class,() -> ObraSocial.instancia(1234,"OSDE",
                "36965115-5", "410","4319531",
                null,"Belgrano 1236" ));
        //Assert
        Assertions.assertEquals("El email es obligatorio", exceptionVacio.getMessage());
        Assertions.assertEquals("El email es obligatorio", exceptionNull.getMessage());
    }
    @Test
    void instancia_FaltaDireccion_InstanciaObraSocial(){
        //Arrange
        //Act
        Exception exceptionVacio = Assertions.assertThrows(ObraSocialIncompletoExceptions.class,() -> ObraSocial.instancia(1234,"OSDE",
                "36965115-0", "410","4319531",
                "miobrasocial@osde.com","" ));
        Exception exceptionNull = Assertions.assertThrows(PacienteIncompletoExceptions.class,() -> ObraSocial.instancia(1234,"OSDE",
                "36965115-5", "410","4319531",
                "miobrasocial@osde.com",null ));
        //Assert
        Assertions.assertEquals("La direccion es obligatoria", exceptionVacio.getMessage());
        Assertions.assertEquals("La direccion es obligatoria", exceptionNull.getMessage());
    }
}
