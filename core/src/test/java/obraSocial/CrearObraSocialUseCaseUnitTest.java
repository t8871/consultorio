package obraSocial;


import obraSocial.exceptions.ObraSocialYaExisteExceptions;
import obraSocial.input.CrearObraSocialInput;
import obraSocial.interactor.CrearObraSocialInteractor;
import obraSocial.modelo.ObraSocial;
import obraSocial.output.CrearObraSocialRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CrearObraSocialUseCaseUnitTest {
    @Mock
    CrearObraSocialRepository crearObraSocialRepository;

    @Test
    void creaObraSocial_ObraSocialNoExiste_ObraSocialMedico(){
        ObraSocial miObraSocial = ObraSocial.instancia(165,"Galeno",
                "49025661-3","550","4489579","galeno@obrasocial",
                "Las Heras 2530");
        CrearObraSocialInput crearObraSocialInteractor = new CrearObraSocialInteractor(crearObraSocialRepository);

        when(crearObraSocialRepository.existePorNombre("Galeno")).thenReturn(false);
        when(crearObraSocialRepository.guardarObraSocial(miObraSocial)).thenReturn(3);

        Assertions.assertEquals(2,crearObraSocialInteractor.crearObraSocial(miObraSocial));
    }
    @Test
    void creaObraSocial_ObraSocialYaExiste_ObraSocialYaExisteException(){
        ObraSocial miObraSocial = ObraSocial.instancia(165,"Galeno",
                "49025661-3","550","4489579","galeno@obrasocial",
                "Las Heras 2530");
        CrearObraSocialInput crearObraSocialInteractor = new CrearObraSocialInteractor(crearObraSocialRepository);

        verify(crearObraSocialRepository, never()).guardarObraSocial(miObraSocial);

        when(crearObraSocialRepository.existePorNombre("Galeno")).thenReturn(true);

        Assertions.assertThrows(ObraSocialYaExisteExceptions.class,()-> crearObraSocialInteractor.crearObraSocial(miObraSocial));
    }
}
