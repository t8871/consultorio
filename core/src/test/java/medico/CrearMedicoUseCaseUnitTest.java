package medico;

import medico.exceptions.MedicoYaExisteException;
import medico.input.CrearMedicoInput;
import medico.interactor.CrearMedicoInteractor;
import medico.modelo.Medico;
import medico.output.CrearMedicoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CrearMedicoUseCaseUnitTest {

    @Mock
    CrearMedicoRepository crearMedicoRepository;
    @Test
    void creaMedico_MedicoNoExiste_CreaMedico(){
        Medico miMedico = Medico.instancia(963,"Lucas","Lopez","Garcia 56",
                "2615897412","llopez@gmail.com","Dermatologo");
        CrearMedicoInput crearMedicoInteractor = new CrearMedicoInteractor(crearMedicoRepository);

        when(crearMedicoRepository.existePorMatricula(963)).thenReturn(false);
        when(crearMedicoRepository.guardarMedico(miMedico)).thenReturn(2);

        Assertions.assertEquals(2,crearMedicoInteractor.crearMedico(miMedico));
    }
    @Test
    void creaMedico_MedicoYaExiste_MedicoYaExisteException(){
        Medico miMedico = Medico.instancia(963,"Lucas","Lopez","Garcia 56",
                "2615897412","llopez@gmail.com","Dermatologo");

        CrearMedicoInput crearMedicoInteractor = new CrearMedicoInteractor(crearMedicoRepository);

        verify(crearMedicoRepository, never()).guardarMedico(miMedico);

        when(crearMedicoRepository.existePorMatricula(963)).thenReturn(true);

        Assertions.assertThrows(MedicoYaExisteException.class,()-> crearMedicoInteractor.crearMedico(miMedico));
    }
}
