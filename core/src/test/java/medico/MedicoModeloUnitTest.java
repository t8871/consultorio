package medico;

import medico.exceptions.MedicoIncompletoExceptions;
import medico.modelo.Medico;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import paciente.exceptions.PacienteIncompletoExceptions;
import paciente.modelo.Paciente;

import java.time.LocalDate;

public class MedicoModeloUnitTest {
    @Test
    void instancia_TodosLosAtributosCorrectos_InstanciaPaciente(){
        //Arrange
        //Act

        Medico miMedico = Medico.instancia(6541, "Cecilia", "Olleni","25 de mayo 5689",
                "2615236514","colleni@gmail.com","Clinica");

        //Assert
        Assertions.assertNotNull(miMedico);
        Assertions.assertEquals(6541, miMedico.getMatricula());
        Assertions.assertEquals("Cecilia", miMedico.getNombre());
        Assertions.assertEquals("Olleni", miMedico.getApellido());
        Assertions.assertEquals("25 de mayo 5689", miMedico.getDireccion());
        Assertions.assertEquals("2615236514", miMedico.getTelefono());
        Assertions.assertEquals("colleni@gmail.com",miMedico.getEmail());
        Assertions.assertEquals("Clinica", miMedico.getEspecialidad());
    }
    @Test
    void instancia_FaltaNombre_InstanciaMedico(){
        //Arrange
        //Act
        Exception exceptionMedicoVacio = Assertions.assertThrows(MedicoIncompletoExceptions.class,() ->Medico.instancia(6541, "", "Olleni","25 de mayo 5689",
                "2615236514","colleni@gmail.com","Clinica"));
        Exception exceptionMedicoNull = Assertions.assertThrows(MedicoIncompletoExceptions.class,() ->Medico.instancia(6541, null, "Olleni","25 de mayo 5689",
                "2615236514","colleni@gmail.com","Clinica"));
        //Assert
        Assertions.assertEquals("El nombre del medico es obligatorio", exceptionMedicoVacio.getMessage());
        Assertions.assertEquals("El nombre del medico es obligatorio", exceptionMedicoNull.getMessage());
    }
    @Test
    void instancia_FaltaApellido_InstanciaMedico(){
        //Arrange
        //Act
        Exception exceptionMedicoVacio = Assertions.assertThrows(MedicoIncompletoExceptions.class,() ->Medico.instancia(6541, "Cecilia", "","25 de mayo 5689",
                "2615236514","colleni@gmail.com","Clinica"));
        Exception exceptionMedicoNull = Assertions.assertThrows(MedicoIncompletoExceptions.class,() ->Medico.instancia(6541, "Cecilia", null,"25 de mayo 5689",
                "2615236514","colleni@gmail.com","Clinica"));
        //Assert
        Assertions.assertEquals("El apellido del medico es obligatorio", exceptionMedicoVacio.getMessage());
        Assertions.assertEquals("El apellido del medico es obligatorio", exceptionMedicoNull.getMessage());
    }

    @Test
    void instancia_FaltaDireccion_InstanciaMedico(){
        //Arrange
        //Act
        Exception exceptionMedicoVacio = Assertions.assertThrows(MedicoIncompletoExceptions.class,() ->Medico.instancia(6541, "Cecilia", "Olleni","",
                "2615236514","colleni@gmail.com","Clinica"));
        Exception exceptionMedicoNull = Assertions.assertThrows(MedicoIncompletoExceptions.class,() ->Medico.instancia(6541, "Cecilia", "Olleni",null,
                "2615236514","colleni@gmail.com","Clinica"));
        //Assert
        Assertions.assertEquals("La direccion del medico es obligatoria", exceptionMedicoVacio.getMessage());
        Assertions.assertEquals("La direccion del medico es obligatoria", exceptionMedicoNull.getMessage());
    }
    @Test
    void instancia_FaltaTelefono_InstanciaMedico(){
        //Arrange
        //Act
        Exception exceptionMedicoVacio = Assertions.assertThrows(MedicoIncompletoExceptions.class,() ->Medico.instancia(6541, "Cecilia", "Olleni","25 de mayo 5689",
                "","colleni@gmail.com","Clinica"));
        Exception exceptionMedicoNull = Assertions.assertThrows(MedicoIncompletoExceptions.class,() ->Medico.instancia(6541, "Cecilia", "Olleni","25 de mayo 5689",
                null,"colleni@gmail.com","Clinica"));
        //Assert
        Assertions.assertEquals("El telefono del medico es obligatorio", exceptionMedicoVacio.getMessage());
        Assertions.assertEquals("El telefono del medico es obligatorio", exceptionMedicoNull.getMessage());
    }
    @Test
    void instancia_FaltaEspecialidad_InstanciaMedico(){
        //Arrange
        //Act
        Exception exceptionMedicoVacio = Assertions.assertThrows(MedicoIncompletoExceptions.class,() ->Medico.instancia(6541, "Cecilia", "Olleni","25 de mayo 5689",
                "2615236514","colleni@gmail.com",""));
        Exception exceptionMedicoNull = Assertions.assertThrows(MedicoIncompletoExceptions.class,() ->Medico.instancia(6541, "Cecilia", "Olleni","25 de mayo 5689",
                "2615236514","colleni@gmail.com",null));
        //Assert
        Assertions.assertEquals("La especialidad del medico es obligatoria", exceptionMedicoVacio.getMessage());
        Assertions.assertEquals("La especialidad del medico es obligatoria", exceptionMedicoNull.getMessage());
    }
}


