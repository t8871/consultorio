package paciente;

import obraSocial.modelo.ObraSocial;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import paciente.exceptions.PacienteYaExisteException;
import paciente.input.CrearPacienteInput;
import paciente.interactor.CrearPacienteInteractor;
import paciente.modelo.Paciente;
import paciente.output.CrearPacienteRepository;

import java.time.LocalDate;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CrearPacienteUseCaseUnitTest {

    @Mock
    CrearPacienteRepository crearPacienteRepository;
    @Test
    void creaPaciente_PacienteNoExiste_CreaPaciente(){
        Paciente miPaciente = Paciente.instancia(null, "Nara","Torres","49025661",
                LocalDate.of(2008,9,23),"Zapiola486","2616272107",
                "naratorres@gmail.com","ORH+", ObraSocial.instancia(165,"Galeno",
                        "49025661-3","550","4489579","galeno@obrasocial",
                        "Las Heras 2530") );
        CrearPacienteInput crearPacienteInteractor = new CrearPacienteInteractor(crearPacienteRepository);

        when(crearPacienteRepository.existePorDNI("49025661")).thenReturn(false);
        when(crearPacienteRepository.guardarPaciente(miPaciente)).thenReturn(2);

        Assertions.assertEquals(2,crearPacienteInteractor.crearPaciente(miPaciente));
    }
    @Test
    void creaPaciente_PacienteYaExiste_PacienteYaExisteException(){
        Paciente miPaciente = Paciente.instancia(null, "Nara","Torres","49025661",
                LocalDate.of(2008,9,23),"Zapiola486","2616272107",
                "naratorres@gmail.com","ORH+", ObraSocial.instancia(165,"Galeno",
                        "49025661-3","550","4489579","galeno@obrasocial",
                        "Las Heras 2530") );

        CrearPacienteInput crearPacienteInteractor = new CrearPacienteInteractor(crearPacienteRepository);

        verify(crearPacienteRepository, never()).guardarPaciente(miPaciente);

        when(crearPacienteRepository.existePorDNI("49025661")).thenReturn(true);

        Assertions.assertThrows(PacienteYaExisteException.class,()-> crearPacienteInteractor.crearPaciente(miPaciente));
    }
}
