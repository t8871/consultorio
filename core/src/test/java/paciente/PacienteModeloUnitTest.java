package paciente;

import obraSocial.modelo.ObraSocial;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import paciente.exceptions.PacienteIncompletoExceptions;
import paciente.modelo.Paciente;

import java.time.LocalDate;

public class PacienteModeloUnitTest {
    @Test
    void instancia_TodosLosAtributosCorrectos_InstanciaPaciente(){
        //Arrange
        //Act
        Paciente miPaciente = Paciente.instancia(1234, "Daniela", "Triador",
                "36965115", LocalDate.of(1992,5,19),
                "15 de junio 71", "2616075388",
                "daniitriador@gmail.com", "BRH+", ObraSocial.instancia(1234,"OSDE",
                        "36965115-0", "410","4319531","miobrasocial@osde.com","Belgrano 1236" ));

        //Assert
        Assertions.assertNotNull(miPaciente);
        Assertions.assertEquals(1234, miPaciente.getIdPaciente());
        Assertions.assertEquals("Daniela", miPaciente.getNombre());
        Assertions.assertEquals("Triador", miPaciente.getApellido());
        Assertions.assertEquals("36965115", miPaciente.getNroDocumento());
        Assertions.assertEquals("15 de junio 71", miPaciente.getDireccion());
        Assertions.assertEquals("2616075388", miPaciente.getTelefono());
        Assertions.assertEquals("daniitriador@gmail.com", miPaciente.getEmail());
        Assertions.assertEquals("BRH+", miPaciente.getGrupoSanguineo());

    }
    @Test
    void instancia_FaltaNombre_InstanciaPaciente(){
        //Arrange
        //Act
        Exception exceptionVacio = Assertions.assertThrows(PacienteIncompletoExceptions.class,() ->Paciente.instancia(1234, "", "Triador",
                "36965115", LocalDate.of(1992,5,19),
                "15 de junio 71", "2616075388",
                "daniitriador@gmail.com", "BRH+", ObraSocial.instancia(1234,"OSDE",
                        "36965115-0", "410","4319531","miobrasocial@osde.com","Belgrano 1236" )));
        Exception exceptionNull = Assertions.assertThrows(PacienteIncompletoExceptions.class,() -> Paciente.instancia(1234, null, "Triador",
                "36965115", LocalDate.of(1992,5,19),
                "15 de junio 71", "2616075388",
                "daniitriador@gmail.com", "BRH+", ObraSocial.instancia(1234,"OSDE",
                        "36965115-0", "410","4319531","miobrasocial@osde.com","Belgrano 1236" )));
        //Assert
        Assertions.assertEquals("El nombre del paciente es obligatorio", exceptionVacio.getMessage());
        Assertions.assertEquals("El nombre del paciente es obligatorio", exceptionNull.getMessage());
    }
    @Test
    void instancia_FaltaApellido_InstanciaPaciente(){
        //Arrange
        //Act
        Exception exceptionVacio = Assertions.assertThrows(PacienteIncompletoExceptions.class,() ->Paciente.instancia(1234, "Daniela", "",
                "36965115", LocalDate.of(1992,5,19),
                "15 de junio 71", "2616075388",
                "daniitriador@gmail.com", "BRH+",ObraSocial.instancia(1234,"OSDE",
                        "36965115-0", "410","4319531","miobrasocial@osde.com","Belgrano 1236" )));
        Exception exceptionNull = Assertions.assertThrows(PacienteIncompletoExceptions.class,() -> Paciente.instancia(1234, "Daniela", null,
                "36965115", LocalDate.of(1992,5,19),
                "15 de junio 71", "2616075388",
                "daniitriador@gmail.com", "BRH+", ObraSocial.instancia(1234,"OSDE",
                        "36965115-0", "410","4319531","miobrasocial@osde.com","Belgrano 1236" )));
        //Assert
        Assertions.assertEquals("El apellido del paciente es obligatorio", exceptionVacio.getMessage());
        Assertions.assertEquals("El apellido del paciente es obligatorio", exceptionNull.getMessage());
    }

    @Test
    void instancia_FaltaNroDocumento_InstanciaPaciente(){
        //Arrange
        //Act
        Exception exceptionVacio = Assertions.assertThrows(PacienteIncompletoExceptions.class,() ->Paciente.instancia(1234, "Daniela", "Triador",
                "", LocalDate.of(1992,5,19),
                "15 de junio 71", "2616075388",
                "daniitriador@gmail.com", "BRH+", ObraSocial.instancia(1234,"OSDE",
                        "36965115-0", "410","4319531","miobrasocial@osde.com","Belgrano 1236" )));
        Exception exceptionNull = Assertions.assertThrows(PacienteIncompletoExceptions.class,() -> Paciente.instancia(1234, "Daniela", "Triador",
                null, LocalDate.of(1992,5,19),
                "15 de junio 71", "2616075388",
                "daniitriador@gmail.com", "BRH+", ObraSocial.instancia(1234,"OSDE",
                        "36965115-0", "410","4319531","miobrasocial@osde.com","Belgrano 1236" )));
        //Assert
        Assertions.assertEquals("El numero de documento del paciente es obligatorio", exceptionVacio.getMessage());
        Assertions.assertEquals("El numero de documento del paciente es obligatorio", exceptionNull.getMessage());
    }

    @Test
    void instancia_FaltaDireccion_InstanciaPaciente(){
        //Arrange
        //Act
        Exception exceptionVacio = Assertions.assertThrows(PacienteIncompletoExceptions.class,() ->Paciente.instancia(1234, "Daniela", "Triador",
                "36965115", LocalDate.of(1992,5,19),
                "", "2616075388",
                "daniitriador@gmail.com", "BRH+", ObraSocial.instancia(1234,"OSDE",
                        "36965115-0", "410","4319531","miobrasocial@osde.com","Belgrano 1236" )));
        Exception exceptionNull = Assertions.assertThrows(PacienteIncompletoExceptions.class,() -> Paciente.instancia(1234, "Daniela", "Triador",
                "36965115", LocalDate.of(1992,5,19),
                null, "2616075388",
                "daniitriador@gmail.com", "BRH+", ObraSocial.instancia(1234,"OSDE",
                        "36965115-0", "410","4319531","miobrasocial@osde.com","Belgrano 1236" )));
        //Assert
        Assertions.assertEquals("La direccion del paciente es obligatoria", exceptionVacio.getMessage());
        Assertions.assertEquals("La direccion del paciente es obligatoria", exceptionNull.getMessage());
    }
    @Test
    void instancia_FaltaGrupoSanguineo_InstanciaPaciente(){
        //Arrange
        //Act
        Exception exceptionVacio = Assertions.assertThrows(PacienteIncompletoExceptions.class,() ->Paciente.instancia(1234, "Daniela", "Triador",
                "36965115", LocalDate.of(1992,5,19),
                "15 de junio 71", "2616075388",
                "daniitriador@gmail.com", "", ObraSocial.instancia(1234,"OSDE",
                        "36965115-0", "410","4319531","miobrasocial@osde.com","Belgrano 1236" )));
        Exception exceptionNull = Assertions.assertThrows(PacienteIncompletoExceptions.class,() -> Paciente.instancia(1234, "Daniela", "Triador",
                "36965115", LocalDate.of(1992,5,19),
                "15 de junio 71", "2616075388",
                "daniitriador@gmail.com", null, ObraSocial.instancia(1234,"OSDE",
                        "36965115-0", "410","4319531","miobrasocial@osde.com","Belgrano 1236" )));
        //Assert
        Assertions.assertEquals("El grupo sanguineo del paciente es obligatorio", exceptionVacio.getMessage());
        Assertions.assertEquals("El grupo sanguineo del paciente es obligatorio", exceptionNull.getMessage());
    }

}
