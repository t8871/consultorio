# **Consultorio Medico**

Trabajo practico integrador de programación III


# **Nombre**

Consultorio Medico

# Descripcion del proyecto

El fin de este proyecto es administrar un centro médico. Debido a la complejidad del mismo, tan solo se tendrán en cuenta la gestión de turnos y el ABM de pacientes.
Para esto se utilizaran cuatro tablas en MySQl: Paciente, médico, turno y obra social.
Los limites del proyecto serán los siguientes:
    - Cada medico podra tener hasta 10 turnos por dia, con una duracion de 30 minutos cada uno
    - No se podran registrar sobreturnos 
    - Los turnos no son reprogramables
    - Un paciente no podra tener asignado mas de un turno al dia en una misma especialidad y/o mismo medico
    - Un paciente no podrá tener asignado más de dos turnos por dia aunque las consultas sean en distintas especialidades.
    - No se podran cargar pacientes con los siguientes campos nulos: nombre, apellido, dni o grupo sanguineo. 

# Stack Utilizado

Maven, Java, Spring Boot.

# Arquitectura

Clean Architecture - Principios SOLID - Patrones de Diseño

# DER Conceptual

[DER](https://gitlab.com/t8871/consultorio/-/blob/main/consultorio.png)

# Casos de uso

Caso de uso Nº1: Registrar Paciente
Caso de uso Nº2: Modificar Paciente
Caso de uso Nº3: Registrar Turno
Caso de uso Nº4: Modificar Turno

# Autor
Triador, Daniela Andrea

